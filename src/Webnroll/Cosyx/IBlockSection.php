<?php
/**
 * Cosyx Bitrix Extender
 *
 * @version $Id$
 * @author Peredelskiy Aleksey <info@web-n-roll.ru>
 */

namespace Webnroll\Cosyx;

/**
 * Class IBlockSection
 * Методы для работы с разделами Инфоблоков
 *
 * @package Webnroll\Cosyx
 *
 */
class IBlockSection extends Singleton
{
    protected function __construct($args = array())
    {
        \CModule::IncludeModule('iblock');
    }

    /**
     * @return IBlockSection
     */
    public static function getInstance()
    {
        return self::_getInstance(__CLASS__);
    }

    public function getById($id)
    {
        $rs = \CIBlockSection::GetByID($id);
        if ($ar = $rs->GetNext()) {
            return $ar;
        } else {
            return null;
        }
    }

    public function getByIdFull($iblockId, $id)
    {
        return $this->getSingle(
            false,
            array(
                'IBLOCK_ID' => $iblockId,
                'ID' => $id,
            ),
            false,
            array(
                '*', 'UF_*'
            )
        );
    }

    public function getByCode($iblockId, $code)
    {
        return $this->getSingle(
            false,
            array(
                'IBLOCK_ID' => $iblockId,
                'CODE' => $code,
            ),
            false,
            array(
                'ID', 'NAME', 'CODE', 'DESCRIPTION', 'XML_ID'
            )
        );
    }

    public function getByCodeFull($iblockId, $code)
    {
        return $this->getSingle(
            false,
            array(
                'IBLOCK_ID' => $iblockId,
                'CODE' => $code,
            ),
            false,
            array(
                '*', 'UF_*'
            )
        );
    }

    public function getByIdElement($id)
    {
        $rs = \CIBlockSection::GetByID($id);
        if ($ar = $rs->GetNextElement()) {
            return $ar;
        } else {
            return null;
        }
    }

    public function getList($arOrder = array("SORT" => "ASC"),
                            $arFilter = array(),
                            $bIncCnt = false,
                            $arSelect = false,
                            $arNavStartParams = false,
                            $arParams = array()
    )
    {
        $rs = \CIBlockSection::GetList($arOrder, $arFilter, $bIncCnt, $arSelect, $arNavStartParams);
        $rs->SetUrlTemplates('', $arParams["SECTION_URL"], $arParams["IBLOCK_URL"]);
        $rows = array();
        while ($ar = $rs->GetNext()) {
            $rows[] = $ar;
        }

        return $rows;
    }

    public function getListElement($arOrder = array("SORT" => "ASC"),
                                   $arFilter = array(),
                                   $bIncCnt = false,
                                   $arSelect = false,
                                   $arNavStartParams = false,
                                   $arParams = array()
    )
    {
        $rs = \CIBlockSection::GetList($arOrder, $arFilter, $bIncCnt, $arSelect, $arNavStartParams);
        $rs->SetUrlTemplates('', $arParams["SECTION_URL"], $arParams["IBLOCK_URL"]);
        $rows = array();
        while ($ar = $rs->GetNextElement()) {
            $rows[] = $ar;
        }

        return $rows;
    }

    public function getListHash($arOrder = array("SORT" => "ASC"),
                                $arFilter = array(),
                                $bIncCnt = false,
                                $arSelect = false,
                                $arNavStartParams = false,
                                $arParams = array()
    )
    {
        $rs = \CIBlockSection::GetList($arOrder, $arFilter, $bIncCnt, $arSelect, $arNavStartParams);
        $rs->SetUrlTemplates('', $arParams["SECTION_URL"], $arParams["IBLOCK_URL"]);
        $rows = array();
        while ($ar = $rs->GetNext()) {
            $rows[$ar['ID']] = $ar;
        }

        return $rows;
    }

    public function getSingle($arOrder = array("SORT" => "ASC"),
                              $arFilter = array(),
                              $bIncCnt = false,
                              $arSelect = false,
                              $arNavStartParams = false,
                              $arParams = array()
    )
    {
        $rs = \CIBlockSection::GetList($arOrder, $arFilter, $bIncCnt, $arSelect, $arNavStartParams);
        $rs->SetUrlTemplates('', $arParams["SECTION_URL"], $arParams["IBLOCK_URL"]);
        if ($ar = $rs->GetNext()) {
            return $ar;
        } else {
            return null;
        }
    }

    protected function getKey()
    {
        $args = func_get_args();
        return md5(serialize($args));
    }
}