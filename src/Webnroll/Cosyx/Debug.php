<?php
/**
 * Cosyx Bitrix Extender
 *
 * @version $Id$
 * @author Peredelskiy Aleksey <info@web-n-roll.ru>
 */
namespace Webnroll\Cosyx;

/**
 * Class Debug
 * Методы для отладочного вывода
 * 
 * @package Webnroll\Cosyx
 *
 */
class Debug
{
    public static function log($o, $adminOnly = true) {
        global $USER;

        if ($adminOnly && !$USER->IsAdmin()) return;

        echo '<pre>';
        var_dump($o);
        echo '</pre>';
    }
}
