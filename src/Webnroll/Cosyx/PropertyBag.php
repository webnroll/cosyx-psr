<?php
/**
 * Cosyx Bitrix Extender
 *
 * @version $Id$
 * @author Peredelskiy Aleksey <info@web-n-roll.ru>
 */
namespace Webnroll\Cosyx;

/**
 * Class PropertyBag
 * Универсальное хранилище (вместо глобальных переменных)
 *
 * @package Webnroll\Cosyx
 *
 */
class PropertyBag extends Singleton implements IHashable
{
    protected $propertyBag = null;

    protected function __construct($args = array())
    {
        $this->propertyBag = new Hash();
    }

    /**
     * @return PropertyBag
     */
    public static function getInstance()
    {
        return self::_getInstance(__CLASS__);
    }

    /**
     *     IHashable implementation
     */
    public function set()
    {
        $args = func_get_args();
        return call_user_func_array(array($this->propertyBag, 'set'), $args);
    }

    public function get()
    {
        $args = func_get_args();
        return call_user_func_array(array($this->propertyBag, 'get'), $args);
    }

    public function has()
    {
        $args = func_get_args();
        return call_user_func_array(array($this->propertyBag, 'has'), $args);
    }

    public function remove()
    {
        $args = func_get_args();
        return call_user_func_array(array($this->propertyBag, 'remove'), $args);
    }

    public function & getHashRef()
    {
        return $this->propertyBag->getHashRef();
    }
}