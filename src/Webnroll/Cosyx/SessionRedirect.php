<?php
/**
 * Cosyx Bitrix Extender
 *
 * @version $Id$
 * @author Peredelskiy Aleksey <info@web-n-roll.ru>
 */
namespace Webnroll\Cosyx;

/**
 * Class SessionRedirect
 * Редирект с передачей параметров через сессию.
 *
 * @package Webnroll\Cosyx
 *
 */
class SessionRedirect extends Singleton
{
    protected $redirectParams = array();

    protected function __construct($args = array())
    {
        if (isset($_SESSION['csx_redirect_params'])) {
            $this->redirectParams = $_SESSION['csx_redirect_params'];
            unset($_SESSION['csx_redirect_params']);
        }
    }

    /**
     * @return SessionRedirect
     */
    public static function getInstance()
    {
        return self::_getInstance(__CLASS__);
    }

    public function redirect($url, $params = array())
    {
        $_SESSION['csx_redirect_params'] = $params;
        LocalRedirect($url);
    }

    public function get($key)
    {
        return isset($this->redirectParams[$key]) ? $this->redirectParams[$key] : null;
    }

    public function has($key)
    {
        return isset($this->redirectParams[$key]);
    }

    public function getParams()
    {
        return $this->redirectParams;
    }
}