<?php
/**
 * Cosyx Bitrix Extender
 *
 * @version $Id$
 * @author Peredelskiy Aleksey <info@web-n-roll.ru>
 */
namespace Webnroll\Cosyx;

/**
 * Class Cache
 * Класс поддержки кеширования на базе CPHPCache
 *
 * @package Webnroll\Cosyx
 *
 */
class Cache extends Singleton
{
    const CACHE_TIME = 3600;
    const CACHE_PATH = '/cosyx_cache/';

    /** @var \CPHPCache|null */
    protected $cache = null;

    protected function __construct($args = array())
    {
        $this->cache = new \CPHPCache();
    }

    /**
     * @return Cache
     */
    public static function getInstance()
    {
        return self::_getInstance(__CLASS__);
    }

    /**
     *     IHashable implementation
     */
    public function set($key, $value, $cacheTime = null)
    {
        if (!$cacheTime) {
            $cacheTime = self::CACHE_TIME;
        }

        $this->cache->InitCache($cacheTime, $key, self::CACHE_PATH);
        $this->cache->StartDataCache();
        $this->cache->EndDataCache($value);

        return true;
    }

    public function get($key, $cacheTime = null)
    {
        if (!$cacheTime) {
            $cacheTime = self::CACHE_TIME;
        }

        if ($this->cache->InitCache($cacheTime, $key, self::CACHE_PATH)) {
            return $this->cache->GetVars();
        }

        return null;
    }
}