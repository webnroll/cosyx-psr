<?php
/**
 * Cosyx Bitrix Extender
 *
 * @version $Id$
 * @author Peredelskiy Aleksey <info@web-n-roll.ru>
 */
namespace Webnroll\Cosyx;

/**
 * Class Ajax
 * Класс поддержки ajax вызовов компонентов.
 *
 * @package Webnroll\Cosyx
 */
class Ajax
{
    /**
     * Method to handle ajax requests (like ASP.NET UpdatePanel).
     * Reset output buffer and outputs only target component.
     * Usually client script replaces some container content with received data.
     *
     * @param $key
     * @param $component
     * @param $arParams
     */
    public static function handleRequest($key, &$component, &$arParams)
    {
        if (isset($_REQUEST["ajax_{$key}"]) && $_REQUEST["ajax_{$key}"] == 'yes') {
            unset($_REQUEST["ajax_{$key}"]);

            global $APPLICATION;
            $APPLICATION->RestartBuffer();

            $arNewParams = array();
            foreach ($arParams as $key => $value) {
                if (substr($key, 0, 1) == '~') {
                    $arNewParams[substr($key, 1)] = $value;
                }
            }
            $arNewParams['CSX_AJAX_CALL'] = true;

            $APPLICATION->IncludeComponent(
                $component->GetName(),
                $component->GetTemplateName(),
                $arNewParams,
                false,
                array('HIDE_ICONS' => 'Y')
            );

            die();
        }
    }

    public static function isAjax($key) {
        return (isset($_REQUEST["ajax_{$key}"]) && $_REQUEST["ajax_{$key}"] == 'yes');
    }
}