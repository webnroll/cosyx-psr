<?php
/**
 * Cosyx Bitrix Extender
 *
 * @version $Id$
 * @author Peredelskiy Aleksey <info@web-n-roll.ru>
 */
namespace Webnroll\Cosyx;

/**
 * Class ServerUtility
 * Серверные утилиты
 *
 * @package Webnroll\Cosyx
 *
 */
class ServerUtility {
    /**
     * @param $header
     * @param $value
     */
    public static function setHeader($header, $value) {
        header("{$header}: {$value}\n");
    }

    /**
     * @param $filename
     * @param $data
     * @param bool|false $no_contenttype_set
     */
    public static function sendFileToClient($filename, $data, $no_contenttype_set = false)
    {
        if (!$no_contenttype_set) {
            self::setHeader('Content-Type', 'application/ofx');
        }

        self::setHeader('Content-Disposition', "attachment; filename=$filename");
        echo $data;
        exit();
    }

}