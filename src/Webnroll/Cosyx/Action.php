<?php
/**
 * Cosyx Bitrix Extender
 *
 * @package Webnroll\Cosyx
 * @version $Id$
 * @author Peredelskiy Aleksey <info@web-n-roll.ru>
 */
namespace Webnroll\Cosyx;

/**
 * Обработчик действий.
 * Активируется передачей параметра action с формы или в строке запроса.
 *
 */
class Action
{
    /**
     * @param $action
     * @param $fn
     * @param array $params
     */
    public static function handle($action, $fn, $params = array())
    {
        if (isset($_REQUEST['action']) && $_REQUEST['action'] == $action) {
            array_unshift($params, $action);
            call_user_func_array($fn, $params);
        }
    }
}