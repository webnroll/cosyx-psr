<?php
/**
 * Cosyx Bitrix Extender
 *
 * @version $Id$
 * @author Peredelskiy Aleksey <info@web-n-roll.ru>
 */
namespace Webnroll\Cosyx;

use CHTTP;

/**
 * Class Mvc
 * Легковесный MVC.
 * Имеет смысл вызывать из обработчика OnBeforeProlog
 *
 * @package Webnroll\Cosyx
 *
 */
class Mvc {
    private $namespace;

    public function __construct($namespace) {
        $this->namespace = $namespace;
    }

    public function handle() {
        CHTTP::SetStatus("200 OK");
        @define("ERROR_404","N");

        $url = explode('?', $_SERVER['REDIRECT_URL'] ?: $_SERVER['REQUEST_URI']);
        $url = explode('/', $url[0]);
        array_shift($url);
        $className = $this->namespace . "\\" . ucfirst($url[0]) . 'Controller';
        $methodName = count($url)>1 ? $url[1] : 'index';

        if (class_exists($className)) {
            $obj = new $className();
            if (method_exists($obj, $methodName)) {
                $result = $obj->$methodName();
                if ($result) {
                    echo $result;
                }
                exit();
            }
        }
    }
}