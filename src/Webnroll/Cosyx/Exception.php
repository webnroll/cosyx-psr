<?php
/**
 * Cosyx Bitrix Extender
 *
 * @version $Id$
 * @author Peredelskiy Aleksey <info@web-n-roll.ru>
 */
namespace Webnroll\Cosyx;

/**
 * Class Exception
 * Исключение
 *
 * @package Webnroll\Cosyx
 *
 */
class Exception extends \Exception
{
}