<?php
/**
 * Cosix Bitrix Extender
 *
 * @version $Id$
 * @author Peredelskiy Aleksey <info@web-n-roll.ru>
 */
namespace Webnroll\Cosyx;

class HLBlock
{
    /**
     * @param $blockId
     * @return \Bitrix\Main\Entity\DataManager
     */
    public static function getDataClass($blockId) {
        \CModule::IncludeModule('highloadblock');

        $hlBlock = \Bitrix\Highloadblock\HighloadBlockTable::getById($blockId)->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlBlock);
        $entityDataClass = $entity->getDataClass();

        return $entityDataClass;
    }
}
