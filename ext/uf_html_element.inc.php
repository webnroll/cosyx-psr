<?php
// Регистрируем обработчик события главного модуля OnUserTypeBuildList
// Событие создается при построении списка типов пользовательских свойств
AddEventHandler('main', 'OnUserTypeBuildList', array('CUserTypeHtmlElement', 'GetUserTypeDescription'), 5000);

class CUserTypeHtmlElement
{
    function GetUserTypeDescription()
    {
        return array(
            "USER_TYPE_ID" => "uf_html_element",
            "CLASS_NAME"   => "CUserTypeHtmlElement",
            "DESCRIPTION"  => "HTML",
            "BASE_TYPE"    => "string",
        );
    }

    /**
     * Эта функция вызывается при добавлении нового свойства.
     *
     * <p>Эта функция вызывается для конструирования SQL запроса
     * создания колонки для хранения не множественных значений свойства.</p>
     * <p>Значения множественных свойств хранятся не в строках, а столбиках (как в инфоблоках)
     * и тип такого поля в БД всегда text.</p>
     * @param array $arUserField Массив описывающий поле
     * @return string
     * @static
     */
    function GetDBColumnType($arUserField)
    {
        global $DB;
        switch (strtolower($DB->type)) {
            case "mysql":
                return "text";
            case "oracle":
                return "varchar2(2000 char)";
            case "mssql":
                return "varchar(2000)";
        }
    }

    /**
     * Эта функция вызывается перед сохранением метаданных свойства в БД.
     *
     * <p>Она должна "очистить" массив с настройками экземпляра типа свойства.
     * Для того что бы случайно/намеренно никто не записал туда всякой фигни.</p>
     * @param array $arUserField Массив описывающий поле. <b>Внимание!</b> это описание поля еще не сохранено в БД!
     * @return array Массив который в дальнейшем будет сериализован и сохранен в БД.
     * @static
     */
    function PrepareSettings($arUserField)
    {
        return array(
            "DEFAULT_VALUE" => $arUserField["SETTINGS"]["DEFAULT_VALUE"]
        );
    }

    /**
     * Эта функция вызывается при выводе формы настройки свойства.
     *
     * <p>Возвращает html для встраивания в 2-х колоночную таблицу.
     * в форму usertype_edit.php</p>
     * <p>т.е. tr td bla-bla /td td edit-edit-edit /td /tr </p>
     * @param array $arUserField Массив описывающий поле. Для нового (еще не добавленного поля - <b>false</b>)
     * @param array $arHtmlControl Массив управления из формы. Пока содержит только один элемент NAME (html безопасный)
     * @return string HTML для вывода.
     * @static
     */
    function GetSettingsHTML($arUserField = false, $arHtmlControl, $bVarsFromForm)
    {
        $result = '';
        if ($bVarsFromForm) {
            $value = htmlspecialcharsbx($GLOBALS[$arHtmlControl["NAME"]]["DEFAULT_VALUE"]);
        } elseif (is_array($arUserField)) {
            $value = htmlspecialcharsbx($arUserField["SETTINGS"]["DEFAULT_VALUE"]);
        } else {
            $value = "";
        }
        $result .= '
		<tr>
			<td>' . GetMessage("USER_TYPE_STRING_DEFAULT_VALUE") . ':</td>
			<td>
				<input type="text" name="' . $arHtmlControl["NAME"] . '[DEFAULT_VALUE]" size="20"  maxlength="225" value="' . $value . '">
			</td>
		</tr>
		';

        return $result;
    }

    function GetAdminListEditHTML($arUserField, $arHtmlControl)
    {
        if ($arUserField["SETTINGS"]["ROWS"] < 2) {
            return '<input type="text" ' .
            'name="' . $arHtmlControl["NAME"] . '" ' .
            'size="' . $arUserField["SETTINGS"]["SIZE"] . '" ' .
            ($arUserField["SETTINGS"]["MAX_LENGTH"] > 0 ? 'maxlength="' . $arUserField["SETTINGS"]["MAX_LENGTH"] . '" ' : '') .
            'value="' . $arHtmlControl["VALUE"] . '" ' .
            '>';
        } else {
            return '<textarea ' .
            'name="' . $arHtmlControl["NAME"] . '" ' .
            'cols="' . $arUserField["SETTINGS"]["SIZE"] . '" ' .
            'rows="' . $arUserField["SETTINGS"]["ROWS"] . '" ' .
            ($arUserField["SETTINGS"]["MAX_LENGTH"] > 0 ? 'maxlength="' . $arUserField["SETTINGS"]["MAX_LENGTH"] . '" ' : '') .
            '>' . $arHtmlControl["VALUE"] . '</textarea>';
        }
    }

    /**
     * Эта функция вызывается при выводе формы редактирования значения свойства.
     *
     * <p>Возвращает html для встраивания в ячейку таблицы.
     * в форму редактирования сущности (на вкладке "Доп. свойства")</p>
     * <p>Элементы $arHtmlControl приведены к html безопасному виду.</p>
     * @param array $arUserField Массив описывающий поле.
     * @param array $arHtmlControl Массив управления из формы. Содержит элементы NAME и VALUE.
     * @return string HTML для вывода.
     * @static
     */
    function GetEditFormHTML($arUserField, $arHtmlControl)
    {
        //	if($arUserField["ENTITY_VALUE_ID"]<1 && strlen($arUserField["SETTINGS"]["DEFAULT_VALUE"])>0)
        //$arHtmlControl["VALUE"] = htmlspecialcharsbx($arUserField["SETTINGS"]["DEFAULT_VALUE"]);

        $id = preg_replace("/[^a-z0-9]/i", '', $arHtmlControl['NAME']);

        ob_start();
        ?>
        <table><?
            if (COption::GetOptionString("iblock", "use_htmledit", "Y") == "Y" && CModule::IncludeModule("fileman")):
                ?>
                <tr>
                    <td colspan="2" align="center">
                        <input type="hidden" name="<?= $arHtmlControl["NAME"] ?>" value=""/>
                        <? if (is_null($arUserField["VALUE"])) {
                            $arHtmlControl["VALUE"] = $arUserField["SETTINGS"]["DEFAULT_VALUE"];
                        } ?>
                        <?
                        $text_type = preg_replace("/([^a-z0-9])/is", "_", $arHtmlControl["NAME"] . "[TYPE]");
                        CFileMan::AddHTMLEditorFrame(
                            $arHtmlControl["NAME"],
                            $arHtmlControl["VALUE"],
                            $text_type,
                            strToLower("html"),
                            $arUserField["SETTINGS"]['height'],
                            "N",
                            0,
                            "",
                            ""
                        );
                        ?>
                    </td>
                </tr>
                <?
            else:?>
                <tr>
                    <td><? echo GetMessage("IBLOCK_DESC_TYPE") ?></td>
                    <td>
                        <input type="radio" name="<?= $arHtmlControl["VALUE"] ?>[TYPE]"
                               id="<?= $arHtmlControl["VALUE"] ?>[TYPE][TEXT]"
                               value="text" <? if ($arHtmlControl["TYPE"] != "html") echo " checked" ?>>
                        <label for="<?= $arHtmlControl["VALUE"] ?>[TYPE][TEXT]"><? echo GetMessage(
                                "IBLOCK_DESC_TYPE_TEXT"
                            ) ?></label> /
                        <input type="radio" name="<?= $arHtmlControl["VALUE"] ?>[TYPE]"
                               id="<?= $arHtmlControl["VALUE"] ?>[TYPE][HTML]"
                               value="html"<? if ($arHtmlControl["TYPE"] == "html") echo " checked" ?>>
                        <label for="<?= $arHtmlControl["VALUE"] ?>[TYPE][HTML]"><? echo GetMessage(
                                "IBLOCK_DESC_TYPE_HTML"
                            ) ?></label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                    <textarea cols="60" rows="10" name="<?= $arHtmlControl["VALUE"] ?>[TEXT]"
                              style="width:100%"><?= $arHtmlControl["TEXT"] ?></textarea></td>
                </tr>
            <?endif;
            if (($arHtmlControl["WITH_DESCRIPTION"] == "Y") && ('' != trim($arHtmlControl["DESCRIPTION"]))):?>
                <tr>
                    <td colspan="2">
                    <span title="<? echo GetMessage("IBLOCK_PROP_HTML_DESCRIPTION_TITLE") ?>"><? echo GetMessage(
                            "IBLOCK_PROP_HTML_DESCRIPTION_LABEL"
                        ) ?>:<input type="text" name="<?= $arHtmlControl["DESCRIPTION"] ?>"
                                    value="<?= $arHtmlControl["DESCRIPTION"] ?>" size="18"></span>
                    </td>
                </tr>
            <?endif; ?>
        </table>
        <?
        $return = ob_get_contents();
        ob_end_clean();

        return $return;

    }

    /**
     * Эта функция вызывается при выводе значения свойства в списке элементов.
     *
     * <p>Возвращает html для встраивания в ячейку таблицы.</p>
     * <p>Элементы $arHtmlControl приведены к html безопасному виду.</p>
     * @param array $arUserField Массив описывающий поле.
     * @param array $arHtmlControl Массив управления из формы. Содержит элементы NAME и VALUE.
     * @return string HTML для вывода.
     * @static
     */
    function GetAdminListViewHTML($arUserField, $arHtmlControl)
    {
        if (strlen($arHtmlControl["VALUE"]) > 0) {
            return $arHtmlControl["VALUE"];
        } else {
            return '&nbsp;';
        }
    }
}

/*class CUserTypeHtmlElement
{
    // ---------------------------------------------------------------------
    // Общие параметры методов класса:
    // @param array $arUserField - метаданные (настройки) свойства
    // @param array $arHtmlControl - массив управления из формы (значения свойств, имена полей веб-форм и т.п.)
    // ---------------------------------------------------------------------

    // Функция регистрируется в качестве обработчика события OnUserTypeBuildList
    function GetUserTypeDescription()
    {
        return array(
            // уникальный идентификатор
            'USER_TYPE_ID' => 'uf_html_element',
            // имя класса, методы которого формируют поведение типа
            'CLASS_NAME' => 'CUserTypeHtmlElement',
            // название для показа в списке типов пользовательских свойств
            'DESCRIPTION' => 'HTML',
            // базовый тип на котором будут основаны операции фильтра
            'BASE_TYPE' => 'string',
            'PROPERTY_TYPE' => 'HTML'
        );
    }

    // Функция вызывается при добавлении нового свойства
    // для конструирования SQL запроса создания столбца значений свойства
    // @return string - SQL
    function GetDBColumnType($arUserField)
    {
        switch (strtolower($GLOBALS['DB']->type)) {
            case 'mysql':
                return 'text';
                break;
            case 'oracle':
                return 'varchar(4000)';
                break;
            case 'mssql':
                return "nvarchar(max)";
                break;
        }
    }

    // Функция вызывается перед сохранением метаданных (настроек) свойства в БД
    // @return array - массив уникальных метаданных для свойства, будет сериализован и сохранен в БД
    function PrepareSettings($arUserField)
    {
        return array();
    }

    // Функция вызывается при выводе формы метаданных (настроек) свойства
    // @param bool $bVarsFromForm - флаг отправки формы
    // @return string - HTML для вывода
    function GetSettingsHTML($arUserField = false, $arHtmlControl, $bVarsFromForm)
    {
        return '';
    }

    // Функция валидатор значений свойства
    // вызвается в $GLOBALS['USER_FIELD_MANAGER']->CheckField() при добавлении/изменении
    // @param array $value значение для проверки на валидность
    // @return array массив массивов ("id","text") ошибок
    function CheckFields($arUserField, $value)
    {
        $aMsg = array();
        return $aMsg;
    }

    // Функция вызывается при выводе формы редактирования значения свойства
    // @return string - HTML для вывода
    function GetEditFormHTML($arUserField, $arHtmlControl)
    {
        // html поля веб-формы для текущего значения
        $sReturn .= '<div>' . CUserTypeHtmlElement::_getItemFieldHTML($arHtmlControl['VALUE'], $iIBlockId, $arElements, $arHtmlControl['NAME']) . '</div>';
        return $sReturn;
    }

    // Функция вызывается при выводе формы редактирования значения множественного свойства
    // @return string - HTML для вывода
    function GetEditFormHTMLMulty($arUserField, $arHtmlControl)
    {
        $sReturn .= '<div>' . CUserTypeHtmlElement::_getItemFieldHTML($arHtmlControl['VALUE'], $iIBlockId, $arElements, $arHtmlControl['NAME']) . '</div>';
        return $sReturn;
    }

    // Функция вызывается при выводе фильтра на странице списка
    // @return string - HTML для вывода
    function GetFilterHTML($arUserField, $arHtmlControl)
    {
        return CUserTypeHtmlElement::GetEditFormHTML($arUserField, $arHtmlControl);
    }

    // Функция вызывается при выводе значения свойства в списке элементов
    // @return string - HTML для вывода
    function GetAdminListViewHTML($arUserField, $arHtmlControl)
    {
        return ' ';
    }

    // Функция вызывается при выводе значения множественного свойства в списке элементов
    // @return string - HTML для вывода
    function GetAdminListViewHTMLMulty($arUserField, $arHtmlControl)
    {
        return ' ';
    }

    // Функция вызывается при выводе значения свойства в списке элементов в режиме редактирования
    // @return string - HTML для вывода
    function GetAdminListEditHTML($arUserField, $arHtmlControl)
    {
        return CUserTypeHtmlElement::GetEditFormHTML($arUserField, $arHtmlControl);
    }

    // Функция вызывается при выводе множественного значения свойства в списке элементов в режиме редактирования
    // @return string - HTML для вывода
    function GetAdminListEditHTMLMulty($arUserField, $arHtmlControl)
    {
        return CUserTypeHtmlElement::GetEditFormHTML($arUserField, $arHtmlControl);
    }

    // Функция должна вернуть представление значения поля для поиска
    // @return string - посковое содержимое
    function OnSearchIndex($arUserField)
    {
        if (is_array($arUserField['VALUE'])) {
            return implode("\r\n", $arUserField['VALUE']);
        } else {
            return $arUserField['VALUE'];
        }
    }

    // Функция вызывается перед сохранением значений в БД
    // @param mixed $value - значение свойства
    // @return string - значение для вставки в БД
    function OnBeforeSave($arUserField, $value)
    {
        return $value;
    }

    // Функция генерации html для поля редактирования свойства
    // @param int $iValue - значение свойства
    // @param int $iIBlockId - ID информационного блока для поиска элементов по умолчанию
    // @param array $arElements - массив элементов инфоблока с ключами = идентификаторам элементов инфоблока
    // @param string $sFieldName - имя для поля веб-формы
    // @param string $sMulty - n|y - поэлементная (n) или множественная вставка значений
    // @return string - HTML для вывода
    // @private
    function _getItemFieldHTML($iValue, $iIBlockId, $arElements, $sFieldName, $sMulty = 'n')
    {
        $sReturn = '';
        $sReturn = '<textarea cols="80" rows="12" name="' . $sFieldName . '">' . $iValue . '</textarea>';
        return $sReturn;
    }

}*/